docker volume create --driver local \
      --opt type=nfs \
      --opt o=nfsvers=4,addr=192.168.1.45,rw \
      --opt device=:/volumes/kafka \
      kafka-volume