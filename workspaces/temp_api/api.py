from flask import Flask
from flask_restful import reqparse, abort, Api, Resource

app = Flask(__name__)
api = Api(app)



def abort_if_device_doesnt_exist(device_id):
    if device_id not in DEVICES:
        abort(404, message="Device {} doesn't exist".format(device_id))

parser = reqparse.RequestParser()
parser.add_argument('device_name')
parser.add_argument('username')
parser.add_argument('password')
parser.add_argument('faceid')


DEVICES = {
    'device1': {'device_name': 'Camera trệt'},
    'device2': {'device_name': 'Camera sân thượng'},
    'device3': {'device_name': 'Camera lầu 1'},
}
class DeviceAPI(Resource):
    def get(self, device_id):
        abort_if_device_doesnt_exist(device_id)
        return DEVICES[device_id]

    def delete(self, device_id):
        abort_if_device_doesnt_exist(device_id)
        del DEVICES[device_id]
        return '', 204

    def put(self, device_id):
        args = parser.parse_args()
        device = {'device_name': args['device_name']}
        DEVICES[device_id] = device
        return device, 201
class DeviceListAPI(Resource):
    def get(self):
        return DEVICES

    def post(self):
        args = parser.parse_args()
        device_id = int(max(DEVICES.keys()).lstrip('device')) + 1
        device_id = 'device%i' % device_id
        DEVICES[device_id] = {'device_name': args['device_name']}
        return DEVICES[device_id], 201


SERVICES = {
    "service1": {"service_name": "Nhận diện người"},
    "service2": {"service_name": "Nhận diện lửa"},
    "service3": {"service_name": "Nhận diện đột nhập"},
}
class ServiceListAPI(Resource):
    def get(self):
        return SERVICES

    def post(self):
        args = parser.parse_args()
        service_id = int(max(SERVICES.keys()).lstrip('service')) + 1
        service_id = 'service%i' % service_id
        SERVICES[service_id] = {'service_name': args['service_name']}
        return SERVICES[service_id], 201


USER = {
    "username": "",
    "password": "",
    "faceid": ""
}
IS_LOGIN = False
class LoginAPI(Resource):
    def post(self):
        args = parser.parse_args()
        if args.get("faceid") is not None:
            IS_LOGIN = True
            USER["faceid"] = args.get("faceid").strip()
            return USER, 200
        else:
            if args.get("username") is not None and args.get("password") is not None:
                IS_LOGIN = True
                USER["username"] = args.get("username").strip()
                USER["password"] = args.get("password").strip()
                return USER, 200
        return {"msg": "authenticated fail", "status": 501}, 200

class LogoutAPI(Resource):
    def post(self):
        args = parser.parse_args()
        if IS_LOGIN:
            IS_LOGIN = False
        return {"msg": "logout successfully", "status": 200}, 200
 

class SignUpAPI(Resource):
    def post(self):
        args = parser.parse_args()
        if args.get("username") is not None and args.get("password") is not None:
            IS_LOGIN = True
            USER["username"] = args.get("username").strip()
            USER["password"] = args.get("password").strip()
            return USER, 200
        return USER, 200


##
## Actually setup the Api resource routing here
##
api.add_resource(LoginAPI, '/login')
api.add_resource(LogoutAPI, '/logout')
api.add_resource(SignUpAPI, '/signup')
api.add_resource(DeviceListAPI, '/devices')
api.add_resource(DeviceAPI, '/devices/<device_id>')
api.add_resource(ServiceListAPI, '/services')


if __name__ == '__main__':
    app.run(debug=True)